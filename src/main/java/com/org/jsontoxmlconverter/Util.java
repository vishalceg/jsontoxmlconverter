/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 *
 * @author cb-vishal
 */
public class Util {

    public static boolean isFileExists(String path) {
        File file = new File(path);
        return file.isFile();
    }

    public static boolean isValidFilePath(String path) {
        try {
            Paths.get(path);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
         File file = new File(path);
        return !file.isDirectory();
    }

}
