/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter.impl;

import com.org.jsontoxmlconverter.impl.Node;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author cb-vishal
 */
public class NodeToXmlBuilder {

    private final Node node;

    private NodeToXmlBuilder(Node node) {
        this.node = node;
    }

    public static Document build(Node node) throws Exception {
        NodeToXmlBuilder xmlBuilder = new NodeToXmlBuilder(node);
        return xmlBuilder.createXmlDocument();
    }

    public Document createXmlDocument() throws Exception {
        DocumentBuilderFactory dbFactory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.newDocument();
        convert(doc, null, node);
        return doc;
//        TransformerFactory transformerFactory = TransformerFactory.newInstance();
//        Transformer transformer = transformerFactory.newTransformer();
//        DOMSource source = new DOMSource(doc);
//        StreamResult consoleResult = new StreamResult(System.out);
//        transformer.transform(source, consoleResult);
    }

    private void convert(Document doc, Element rootEle, Node root) {
        Element element = createElement(doc, root);
        if (rootEle == null) {
            doc.appendChild(element);
        } else {
            rootEle.appendChild(element);
        }
        for (Node n : root.getChild()) {
            convert(doc, element, n);// recursive call to handle child nodes
        }
    }

    // create xml node from node
    private Element createElement(Document doc, Node node) {
        Element ele = doc.createElement(node.type().name().toLowerCase());
        if (node.key() != null) {
            Attr attr = doc.createAttribute("name");
            attr.setValue(node.key());
            ele.setAttributeNode(attr);
        }
        if (node.value() != null) {
            ele.appendChild(doc.createTextNode(node.value()));
        }
        return ele;
    }
}
