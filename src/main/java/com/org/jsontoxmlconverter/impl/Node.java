/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter.impl;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cb-vishal
 */
public class Node {

    private Type type;
    private String key;
    private String value;
    private List<Node> childList;

    public Node(Type type) {
        this(type, null, null);
    }

    public Node(Type type, String value) {
        this(type, null, value);
    }

    public Node(Type type, String key, String value) {
        this.type = type;
        this.key = key;
        this.value = value;
        this.childList = new ArrayList<>();
    }

    public Node type(Type type) {
        this.type = type;
        return this;
    }

    public Node key(String key) {
        this.key = key;
        return this;
    }

    public Node value(String value) {
        this.value = value;
        return this;
    }

    public Type type() {
        return type;
    }

    public String key() {
        return key;
    }

    public String value() {
        return value;
    }

    public Node addChild(Node child) {
        this.childList.add(child);
        return this;
    }

    public List<Node> getChild() {
        return childList;
    }

}
