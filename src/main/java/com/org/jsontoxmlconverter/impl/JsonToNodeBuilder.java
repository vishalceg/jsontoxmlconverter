/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter.impl;

import java.io.Reader;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author cb-vishal
 */
public class JsonToNodeBuilder {

    private JsonToNodeBuilder() {
    }

    public static Node build(String jsonString) throws Exception {
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(jsonString);
            JsonToNodeBuilder nodeBuilder = new JsonToNodeBuilder();
            return nodeBuilder.prepareNode(obj);
        } catch (Exception ex) {
            throw ex;
        }
    }
    
     public static Node build(Reader reader) throws Exception {
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(reader);
            JsonToNodeBuilder nodeBuilder = new JsonToNodeBuilder();
            return nodeBuilder.prepareNode(obj);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private Node prepareNode(Object obj) throws Exception {
        Type type = getType(obj);
        Node node = new Node(type);
        if (type == Type.Array) {
            handleJsonArry(node, (JSONArray) obj);
        } else if (type == Type.Object) {
            handleJsonObj(node, (JSONObject) obj);
        } else {
            node.value(obj != null ? obj.toString() : null);
        }
        return node;
    }

    private void handleJsonObj(Node node, JSONObject obj) {
        for (Iterator iterator = obj.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            Object o = obj.get(key);
            Type type = getType(o);
            Node child = null;
            if (type == Type.Null || type == Type.Boolean || type == Type.Number || type == Type.String) {
                child = new Node(type, key, type == Type.Null ? null : o.toString());
                node.addChild(child);
            } else {
                child = new Node(type, key, null);
                node.addChild(child);
                if (type == Type.Object) {
                    handleJsonObj(child, (JSONObject) o);
                } else {
                    handleJsonArry(child, (JSONArray) o);
                }
            }

        }
    }

    private void handleJsonArry(Node node, JSONArray arr) {
        for (int i = 0; i < arr.size(); i++) {
            Object o = arr.get(i);
            Type type = getType(o);
            Node child = null;
            if (type == Type.Null || type == Type.Boolean || type == Type.Number || type == Type.String) {
                child = new Node(type, type == Type.Null ? null : o.toString());
                node.addChild(child);
            } else {
                child = new Node(type);
                node.addChild(child);
                if (type == Type.Object) {
                    handleJsonObj(child, (JSONObject) o);
                } else {
                    handleJsonArry(child, (JSONArray) o);
                }
            }

        }
    }

    private static Type getType(Object obj) {
        if (obj == null) {
            return Type.Null;
        } else if (obj instanceof JSONObject) {
            return Type.Object;
        } else if (obj instanceof JSONArray) {
            return Type.Array;
        } else if (obj instanceof Long) {
            return Type.Number;
        } else if (obj instanceof Boolean) {
            return Type.Boolean;
        }
        return Type.String;
    }

}
