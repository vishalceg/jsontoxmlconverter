/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter;

/**
 *
 * @author cb-vishal
 */
public interface XMLJSONConverter {
    
    void convertJSONtoXML(String source, String destination) throws Exception;
    
     default void convertXMLtoJSON(String source, String destination) {
         throw new UnsupportedOperationException("Not yet supported");
     }
     
}
