/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter;

/**
 *
 * @author cb-vishal
 */
public class App {
    
    public static void main(String ar[]) throws Exception{
        if(ar.length != 2){
            System.out.println("Input format: <source file path> <destination file path>");
            System.exit(0);
        }
        ConverterFactory factory= new ConverterFactory();
        XMLJSONConverter converter = factory.getConverterInst();
        converter.convertJSONtoXML(ar[0], ar[1]);
        System.out.println("");
        System.out.println("Completed!!!!!");
        System.exit(0);
    }
    
}
