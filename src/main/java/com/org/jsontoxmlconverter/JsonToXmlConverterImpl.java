/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.jsontoxmlconverter;

import com.org.jsontoxmlconverter.impl.JsonToNodeBuilder;
import com.org.jsontoxmlconverter.impl.Node;
import com.org.jsontoxmlconverter.impl.NodeToXmlBuilder;
import java.io.File;
import java.io.FileReader;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

/**
 *
 * @author cb-vishal
 */
public class JsonToXmlConverterImpl implements XMLJSONConverter {

    @Override
    public void convertJSONtoXML(String source, String destination) throws Exception {
        if (!Util.isFileExists(source)) {
            System.out.println("File not found-" + source);
            return;
        }
        if (!Util.isValidFilePath(destination)) {
            System.out.println("Not valid path-" + destination + ". Please provide valid path with file name");
            return;
        }
        FileReader reader = new FileReader(source);
        Node node = JsonToNodeBuilder.build(reader);
        Document xmlDoc = NodeToXmlBuilder.build(node);
        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(xmlDoc);
        StreamResult result = new StreamResult(new File(destination));
        transformer.transform(domSource, result);

        // Output to console for testing
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(domSource, consoleResult);
    }

}
